# Bootstrapping and joining a permissioned IPFS network

We present how to bootstrap and join to a permissioned/private or public IPFS network using IPFS Docker image.

Prerequisite:

- [Install Docker](https://docs.docker.com/engine/install/)

See: https://docs.google.com/document/d/1bQAzMHDSdAEgQryJRT13ab5mYhu2wAQkKQZUjnaGr_M/edit# section: Demo

## Set up IPFS

1. Download the scripts from TBD

2. Check that swarm/swarm.key exists. If swarm key is missing, add swarm key of a network below to the swarm directory. `swarm.key` MUST contain 

```text
/key/swarm/psk/1.0.0/
/base16/
2c6c69741f95ed59f5a661c76d21d5befb6f95422b9ea6d860d257857755b824
```

3. Start IPFS container by running `./run-docker-ipfs.sh` (default ports are: 14001 for the P2P connection, 15001 for APIs and 18080 for the gateway)

4. Update the bootstrap nodes by running:

```bash
./ipfs.sh bootstrap rm all
./ipfs.sh bootstrap add "/ip4/91.240.216.80/tcp/9001/p2p/12D3KooWAqMbnbWPZ6NGVFuxLvvfMoVF2MbAtosLL9nkCYvUraHt" "/ip4/91.240.216.81/tcp/9001/p2p/12D3KooWFByj7GYyQtSYs7aGovzCkCsxJ56UyfASPiUGvusqbPy8" "/ip4/91.240.216.114/tcp/443/p2p/12D3KooWSSBaGtGuLiciQzeq4cQpPqtbp96XSDbfCxpGx7tenrpA" "/ip4/91.240.216.200/tcp/443/p2p/12D3KooWCwWCt42Z5SLpT9QzESLXx962TuyqvX7SrqE8Tway4qCm" "/ip4/91.240.216.201/tcp/9001/p2p/12D3KooWED7tZH3H2JFHHF7kGq2pyNtq7JG9TunjjysCMXCULepV"
```

5. Check if the bootstrap nodes were updated correctly

```bash
./ipfs.sh bootstrap list
```

6. Check that you're connected to the peers by running

```bash
./ipfs.sh swarm peers
```
If the response is empty, please check the IPFS log (`./logs-docker-ipfs.sh`) or your firewall settings (at least port 443 needs to be open for outbound connections).

7. Enable stream mounting and auto-relay

```bash
./ipfs.sh config --json Experimental.Libp2pStreamMounting true
./ipfs.sh config --json Swarm.EnableRelayHop false
./ipfs.sh config --json Swarm.EnableAutoRelay true
./ipfs.sh config --json Swarm.DisableNatPortMap false
```

9. Start libp2p stream listener

```bash
./ipfs.sh p2p listen /x/didcomm/v1/demo /ip4/127.0.0.1/tcp/12000
```

This will start a libp2p stream listener for protocol `/x/didcomm/v1/demo` and will forward the communication to `localhost:12000`

**Important remark**
Protocol `x/didcomm/v1/demo` is an internal protocol name and can be arbitrary. Libp2p stream doesn't need to know anything about the DIDComm protocol.

10. Exchange IPFS peer ids

Perform steps 1-9 on two different servers (or same server with different ports or networks). To establish a connection, we need to exchange peer ids, that we retrieve as:

```bash
./ipfs.sh id
```

Peer ID has the following form: `12D3KooW...`

11. Test the connection

```
./ipfs.sh p2p forward /x/didcomm/v1/demo /ip4/0.0.0.0/tcp/12001 /p2p/<peer-id of the node we want to connect with>
```