#!/bin/bash

#
# Run IPFS commands
# Usage: ./ipfs.sh <IPFS CLI args>
#

container_name="ipfs_test"

echo "[*] Executing: ipfs $@"
sudo docker exec ${container_name} ipfs $@
echo "[*] Done"
