#!/bin/bash

#
# Start IPFS Docker Container
# Usage: ./start-docker-ipfs.sh
#

set -e

container_name="ipfs_test"

echo "[*] Starting container: ${container_name}"
sudo docker start ${container_name}
echo "[*] Done"