#!/bin/bash

echo "########################"
echo "Stop ipfs_test"
echo "########################"
./stop-docker-ipfs.sh

sleep 3s

echo "########################"
echo "Remove ipfs dir"
echo "########################"
./clean.sh

sleep 3s


echo "########################"
echo "Run docker"
echo "########################"
./run-docker-ipfs.sh

sleep 3s
echo "########################"
echo "Update the bootstrap node list"
echo "########################"
./ipfs.sh bootstrap rm all
./ipfs.sh bootstrap add "/ip4/91.240.216.80/tcp/9001/p2p/12D3KooWAqMbnbWPZ6NGVFuxLvvfMoVF2MbAtosLL9nkCYvUraHt" "/ip4/91.240.216.81/tcp/9001/p2p/12D3KooWFByj7GYyQtSYs7aGovzCkCsxJ56UyfASPiUGvusqbPy8" "/ip4/91.240.216.114/tcp/443/p2p/12D3KooWSSBaGtGuLiciQzeq4cQpPqtbp96XSDbfCxpGx7tenrpA" "/ip4/91.240.216.200/tcp/443/p2p/12D3KooWCwWCt42Z5SLpT9QzESLXx962TuyqvX7SrqE8Tway4qCm" "/ip4/91.240.216.201/tcp/9001/p2p/12D3KooWED7tZH3H2JFHHF7kGq2pyNtq7JG9TunjjysCMXCULepV"

sleep 3s
echo "########################"
echo "List the bootstrap nodes"
echo "########################"
 
./ipfs.sh bootstrap list
 
 
sleep 3s
echo "########################"
echo "Update the configuration"
echo "########################"
 

./ipfs.sh config --json Experimental.Libp2pStreamMounting true
./ipfs.sh config --json Swarm.EnableRelayHop false
./ipfs.sh config --json Swarm.EnableAutoRelay true
./ipfs.sh config --json Swarm.DisableNatPortMap false

sleep 3s
echo "########################"
echo "Start a listener on libp2p /x/didcomm/v1/demo and forward the
communication to localhost: 12000"
echo "########################"
./ipfs.sh p2p listen /x/didcomm/v1/demo /ip4/127.0.0.1/tcp/12000

sleep 3s
echo "########################"
echo "List the peers we're connected to"
echo "########################"
 
./ipfs.sh swarm peers

./ipfs.sh id

