#!/bin/bash

#
# Create IPFS docker container
# Usage: ./run-docker-ipfs.sh
#

set -e

if [ ! -f swarm/swarm.key ];
then
    echo "[ERROR] swarm.key doesn't exist."
    echo "Add swarm key of an existing network to swarm.key file or create a new key by calling ./create-swarm-key.sh"
    echo "Exiting."
    exit -1
fi

# IPFS_DATA mount local dir
IPFS_DATA=$(pwd)/ipfs
# IPFS_STAGING import/export data to IPFS
IPFS_STAGING=$(pwd)/ipfs/staging
# We mount the current working dir so we can copy the swarm.key
IPFS_SWARM=$(pwd)/swarm

PORT_DIDCOMM_ADMIN_ENDPOINT=11000
PORT_DIDCOMM_HL_CLOUD_AGENT=12000

container_name="ipfs_test"

sudo docker run -d \
  --name ${container_name} \
  -v $IPFS_DATA:/data/ipfs \
  -v $IPFS_STAGING:/export \
  -v $IPFS_SWARM:/ipfs-swarm \
  -e IPFS_PROFILE=server \
  -e IPFS_SWARM_KEY_FILE=/ipfs-swarm/swarm.key \
  -p 4001:4001 \
  -p 127.0.0.1:5101:5001 \
  -p 127.0.0.1:8180:8080 \
  -p ${PORT_DIDCOMM_ADMIN_ENDPOINT}:${PORT_DIDCOMM_ADMIN_ENDPOINT} \
  -p ${PORT_DIDCOMM_HL_CLOUD_AGENT}:${PORT_DIDCOMM_HL_CLOUD_AGENT} \
  ipfs/go-ipfs:latest
