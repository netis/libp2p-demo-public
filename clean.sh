#!/bin/bash

#
# Remove docker container and ipfs files
#

set -ue

container_name="ipfs_test"

echo "[*] Removing docker image: ${container_name}"
sudo docker rm ${container_name}

# echo "[*] Removing ipfs files"
rm -rf ipfs
