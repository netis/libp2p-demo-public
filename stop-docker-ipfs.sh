#!/bin/bash

#
# Stop IPFS Docker Container
# Usage: ./stop-docker-ipfs.sh
#

set -e

container_name="ipfs_test"

echo "[*] Stopping container: ${container_name}"
sudo docker stop ${container_name}
echo "[*] Done"
